<?php

    class User extends Model{

        public $id;
        public $firstname;
        public $lastname;
        public $email;
        public $password;
        public $created;

        public function select(){

            return $this->db->select("SELECT * FROM users;");
        }

        public function save(){

            return $this->db->save('users', $this);
        }
    }
?>